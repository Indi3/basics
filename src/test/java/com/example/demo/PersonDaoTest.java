package com.example.demo;

import com.example.demo.DAO.PersonDao;
import com.example.demo.entity.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonDaoTest {

    @Autowired
    PersonDao personDao;

    @Test
    public void test(){
        personDao.save(new Person(1,"name","sur", LocalDate.now(), BigDecimal.ONE));
        personDao.save(new Person(2,"name","sur", LocalDate.now(), BigDecimal.ONE));
        personDao.save(new Person(3,"name","sur", LocalDate.now(), BigDecimal.ONE));
        //Assertions.assertThat();
    }
}