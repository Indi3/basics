package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "PERSON")
public class Person {
    @Id
    @Column (name="ID")
    private int id;

    @Column (name="FIRST_NAME")
    private String firstName;

    @Column (name="SURNAME")
    private String surname;

    @Column (name="DATE_OF_BIRTF")
    private LocalDate birthDate;

    @Column (name="AMOUNT_OF_MONEY")
    private BigDecimal money;

    public Person(){

    }

    public Person(int id, String firstName, String surname, LocalDate birthDate, BigDecimal money) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.birthDate = birthDate;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
