package com.example.demo.controller;

import com.example.demo.DTO.PersonDTO;
import com.example.demo.PersonService;
import com.example.demo.entity.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/user")
    public void addUser(@RequestBody PersonDTO person){
        personService.addUser(person);
    }

    @GetMapping("/users")
    public List<Person> getUsers() {
        return personService.getUsers();
    }

    @GetMapping("/kod")
    public ResponseEntity<Object> get() {
        return new ResponseEntity<Object>(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
    }
}
