package com.example.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class Controller {

    public List<Integer> smth = new ArrayList<>();

    @GetMapping("/something")
    public String helloWorld(){
        return "Hello World";
    }

    @GetMapping("/something/[id]/withUrl")
    public String helloWorld2(@PathVariable Integer id ){
        return "hello " + id;
    }

    //@PostMapping("/something")
    //public Entry helloWorld3 (@RequestBody Entry entry){
    //    return entry.getFirst();
    //}

    @GetMapping("/something3")
    public ResponseEntity<Object> something3()
    {
        return ResponseEntity.status(404).body("xD");
    }

    @GetMapping("/something5/{id}")
    public ResponseEntity<Object> something5(@PathVariable Integer id)
    {
        return ResponseEntity.status(id).body(id);
    }

    @PostMapping("/something6/{id}")
    public void something6POST(@PathVariable Integer id){
        smth.add(id);
    }

    @GetMapping("/something6")
    public String something6GET(){
        return "List: " + smth.toString();
    }

    @DeleteMapping("/something6/{index}")
    public void something6Delete(@PathVariable Integer index){
        smth.remove(index);

    }

    @GetMapping("/something6/all/{multi}")
    public void something6bla(@PathVariable Integer multi){
       smth = smth.stream()
                .map(e-> e*multi)
                .collect(Collectors.toList());
    }
}
