package com.example.demo.DTO;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

public class PersonDTO {

    private int id;
    private String firstName;
    private String surname;
    private LocalDate birthDate;
    private BigDecimal money;

    public PersonDTO(int id, String firstName, String surname, LocalDate birthDate, BigDecimal money) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.birthDate = birthDate;
        this.money = money;
    }

    public PersonDTO() {
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public BigDecimal getMoney() {
        return money;
    }
}
