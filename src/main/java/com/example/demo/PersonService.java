package com.example.demo;

import com.example.demo.DAO.PersonDao;
import com.example.demo.DTO.PersonDTO;
import com.example.demo.entity.Person;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PersonService {
    private final PersonDao personDao;
    private final PersonMapper personMapper;

    public PersonService(PersonDao personDao, PersonMapper personMapper) {
        this.personDao = personDao;
        this.personMapper = personMapper;
    }

    public void addUser(PersonDTO person){
        Person personEntity = personMapper.map(person);
        personDao.save(personEntity);
    }

    public List<Person> getUsers(){
        return personDao.findAll();
    }

}
