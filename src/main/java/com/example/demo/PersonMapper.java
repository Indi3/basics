package com.example.demo;

import com.example.demo.DTO.PersonDTO;
import com.example.demo.entity.Person;
import org.springframework.stereotype.Component;

@Component
public class PersonMapper {

    public Person map(PersonDTO personDTO){
        return new Person(personDTO.getId(), personDTO.getFirstName(), personDTO.getSurname(), personDTO.getBirthDate(), personDTO.getMoney());
    }
}
